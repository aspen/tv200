const homeUrl = 'home.html';
const homeRegex = new RegExp('(moz-extension|chrome-extension)://[\\w-]+/' + homeUrl.replace('.', '\\.'));

function closeAllExcept(keepOpenIds) {
  chrome.tabs.query({}, tabs => {
    for (let i = 0; i < tabs.length; i++) {
      if (!keepOpenIds.includes(tabs[i].id) &&
          tabs[i].url !== 'chrome://extensions/') {
        chrome.tabs.remove(tabs[i].id);
      }
    }
  });
}

function withHomeTab(func) {
  chrome.tabs.query({}, tabs => {
    home = null;
    for (let i = 0; i < tabs.length; i++) {
      const url = tabs[i].url;
      if (url.match(homeRegex)) {
        if (home) {
          chrome.tabs.remove(tabs[i].id);
        } else {
          home = tabs[i];
        }
      }
    }
    if (home) {
      func(home);
    } else {
      console.log('Did not find home tab, creating it');
      chrome.tabs.create({
        url: homeUrl,
      }, tab => {
        closeAllExcept([tab.id]);
        func(tab);
      });
    }
  });
}

function fixUrl(url) {
  if (url.startsWith('https:') || url.startsWith('http:') || url.startsWith('file:')) {
    return url;
  } else {
    return 'https://' + url;
  }
}

chrome.runtime.onMessage.addListener(
  (message, sender, sendResponse) => {
    if (message.type === 'ready') {
      chrome.tabs.update(
        sender.tab.id,
        {
          active: true,
        }
      );
      withHomeTab(home => {
        // Closing tabs a second time seems to help clean up pesky pages like YouTube Music that claim there's unsaved changes
        closeAllExcept([home.id, sender.tab.id]);
      });
    } else if (message.type === 'open') {
      // Close all tabs except home
      closeAllExcept([sender.tab.id]);
      // Activate home
      chrome.tabs.update(
        sender.tab.id,
        {
          active: true,
        }
      );
      // Create new tab (not active) loading the new media
      chrome.tabs.create({
        url: fixUrl(message.url),
        active: false,
      });
    } else if (message.type === 'close') {
      // Activate home
      chrome.tabs.update(
        sender.tab.id,
        {
          active: true,
        }
      );
      // Close all tabs except home
      closeAllExcept([sender.tab.id]);
    } else if (message.type === 'quit') {
      closeAllExcept([]);
    } else if (message.type === 'state') {
      // ignore
    } else {
      console.error('Message sent to service worker has invalid type ' + JSON.stringify(message));
    }
  }
);

withHomeTab(() => {});
