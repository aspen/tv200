class AnalogClock {
  constructor(rootElem) {
    this.root = rootElem;
    for (let i = 0; i < 360; i += 30) {
      this.createMark(i, 'clock-mark-container', 'clock-mark');
    }
    this.hour = this.createMark(0, 'clock-hour-container', 'clock-hour-hand');
    this.minute = this.createMark(0, 'clock-minute-container', 'clock-minute-hand');
    this.updateTime();
  }

  createMark(degrees, containerClass, itemClass) {
    const container = document.createElement('div');
    container.classList.add('clock-item-container', containerClass);
    container.style.transform = 'rotate(' + degrees + 'deg)';
    const mark = document.createElement('div');
    mark.classList.add('clock-item', itemClass);
    container.appendChild(mark);
    this.root.appendChild(container);
    return container;
  }

  updateTime() {
    let current = new Date();
    let minDeg = current.getMinutes() * 6;
    let hourDeg = current.getHours() * 30 + current.getMinutes() / 2;
    this.minute.style.transform = 'rotate(' + minDeg + 'deg)';
    this.hour.style.transform = 'rotate(' + hourDeg + 'deg)';
    let msIntoMinute = current.getSeconds() * 1000 + current.getMilliseconds();
    let msRemaining = (1000 * 60) - msIntoMinute;
    setTimeout(() => this.updateTime(), msRemaining + 100);
  }
}

const clock = new AnalogClock(document.getElementById('clock'));
