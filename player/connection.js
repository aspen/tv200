const protocolVersion = 5;
let idAttempts = 0;
const maxIdAttempts = 10;
const networkIdChars = 3;
const pairingCodeChars = 3;
const tokenLen = 12;
let codeCharList = null; // needs to be before call to randomStr()
let pairingCode = randomStr(pairingCodeChars);
const idPrefix = 'tv200-'
const sessionExpiryMs = 1000 * 60 * 60 * 24 * 7;
let peer = null;
let conns = [];

function randomStr(chars) {
  result = '';
  if (!codeCharList) {
    codeCharList = [];
    // Letters
    for (let i = 0; i < 26; i++) {
      const c = String.fromCharCode(65 + i);
      // Omit O because it looks like 0
      if (c !== 'O') {
        codeCharList.push(c);
      }
    }
    // Numbers
    // Omit 0 because it looks like O
    for (let i = 1; i < 10; i++) {
      const c = String.fromCharCode(48 + i);
      codeCharList.push(c);
    }
    console.log('codes may be built from', codeCharList.length, 'characters:', codeCharList.join(''));
  }
  for (let i = 0; i < chars; i++) {
    result += codeCharList[Math.floor(Math.random() * codeCharList.length)];
  }
  return result;
}

function withNetworkId(func) {
  chrome.storage.local.get('networkId', result => {
    networkId = result.networkId;
    if (networkId && typeof networkId === 'string') {
      func(networkId);
    } else {
      idAttempts++;
      if (idAttempts > maxIdAttempts) {
        showError('Could not find available PeerJS ID');
        setStatus('Failed to initialize PeerJS :/');
      } else {
        const networkId = randomStr(networkIdChars);
        // For security reasons, we clear all autoconnect tokens when we change network IDs
        chrome.storage.local.set({networkId: networkId, tokens: {}}, () => {
          func(networkId);
        });
      }
    }
  });
}

function resetNetworkId() {
  if (mediaTabId) {
    chrome.tabs.sendMessage(mediaTabId, {
      type: 'prepare-close',
    });
  }
  chrome.runtime.sendMessage({
    type: 'close',
  });
  mediaTabId = null;
  idAttempts = 0;
  pairingCode = randomStr(pairingCodeChars);
  chrome.storage.local.set({networkId: null}, () => {
    initPeer();
  });
  const errorsElem = document.getElementById('errors');
  while (errorsElem.firstChild) {
    errorsElem.removeChild(errorsElem.firstChild);
  }
  showError('Player reset');
}

// Set up PeerJS
addEventListener('beforeunload', () => {
  if (peer) {
    peer.destroy();
  }
});
function initPeer() {
  const peerIdElem = document.getElementById('peer-id');
  peerIdElem.textContent = '...';
  withNetworkId(networkId => {
    const peerId = idPrefix + networkId;
    if (peer) {
      peer.destroy();
      conns = [];
      updateConnCount();
    }
    peer = new Peer(peerId);
    setStatus('Initializing...');
    peer.on('error', error => {
      if (error.type == 'unavailable-id') {
        showError('PeerJS ID ' + peerId + ' unavailable, trying another...');
        chrome.storage.local.set({networkId: null}, () => {
          initPeer()
        });
      } else {
        showError('PeerJS connection: ' + error.message);
        setStatus('PeerJS Error :/');
      }
    })
    peer.on('open', () => {
      console.log('PeerJS peer created with ID ' + peer.id);
      updateStatus();
      peerIdElem.textContent = networkId + pairingCode;
    })
    peer.on('connection', conn => {
      handleConnection(conn);
    });
  });
}

function cleanOldSessions(callback) {
  // Remove info for all sessions older than sessionExpiryMs
  chrome.storage.local.get({tokens: {}}, storage => {
    const now = Date.now();
    for (const cid in storage.tokens) {
      if (now - storage.tokens[cid].time > sessionExpiryMs) {
        delete storage.tokens[cid];
      }
    }
    chrome.storage.local.set(storage, callback);
  });
}

function handleConnection(conn) {
  // Ping all existing conns in case one of them is the previous version of this one, and has now
  // disconnected
  conns.forEach(pingConn);
  notifyConnAlive(conn);
  // Set after we receive re recognised cid token
  let expectedC2p = null;
  let authed = false;
  const authError = message => {
    showError('Auth error: ' + message);
    conn.close();
  }
  const authSuccess = () => {
    authed = true;
    conns.push(conn);
    updateConnCount();
    const newCidToken = randomStr(tokenLen);
    const newTokens = {
      p2c: randomStr(tokenLen),
      c2p: randomStr(tokenLen),
      time: Date.now(),
    };
    conn.send({
      type: 'authorized',
      code: pairingCode,
      cid: newCidToken,
      p2c: newTokens.p2c,
      c2p: newTokens.c2p,
      version: protocolVersion,
    });
    conn.send(getMediaState());
    updateStatus();
    chrome.storage.local.get({tokens: {}}, storage => {
      storage.tokens[newCidToken] = newTokens;
      chrome.storage.local.set(storage);
    });
  };
  conn.on('close', () => {
    clearTimeout(conn.timeoutId);
    conns = conns.filter(item => {
        return item !== conn;
    });
    updateConnCount();
  });
  conn.on('data', message => {
    if (message.type === 'pair') {
      if (message.code === pairingCode) {
        authSuccess();
      } else {
        authError('incorrect pairing code');
      }
    } else if (message.type === 'autoconnect') {
      if (message.stage === 'cid' && !expectedC2p) {
        chrome.storage.local.get({tokens: {}}, storage => {
          const tokens = storage.tokens[message.token];
          delete storage.tokens[message.token];
          if (!tokens) {
            authError('session unknown or expired');
            return;
          }
          expectedC2p = tokens.c2p;
          chrome.storage.local.set(storage, () => {
            conn.send({
              type: 'autoconnect',
              stage: 'p2c',
              token: tokens.p2c,
            });
          });
        });
      } else if (message.stage === 'c2p' && expectedC2p) {
        if (message.token === expectedC2p) {
          authSuccess();
        } else {
          authError('invalid c2p token');
        }
      } else {
        authError(
          'invalid autoconnect stage, expected ' +
          (c2pToken ? 'c2p' : 'cid') +
          ', got ' + message.stage
        );
      }
    } else if (authed) {
      notifyConnAlive(conn);
      handleControlMessage(conn, message);
    }
  });
}

function pingConn(conn) {
  if (conn.open) {
    console.log('Pinging ', conn.peer );
    conn.send({type: 'ping'});
    clearTimeout(conn.timeoutId);
    conn.timeoutId = setTimeout(() => {
      console.log(conn.peer, 'timed out');
      conn.close();
    }, 5000);
  }
}

function notifyConnAlive(conn) {
  clearTimeout(conn.timeoutId);
  conn.timeoutId = setTimeout(() => {
    pingConn(conn);
  }, 30000);
}

addEventListener('offline', () => {
  showError('Lost network');
  setStatus('Offline :(');
});

addEventListener('online', () => {
  showError('TV back online :)');
  initPeer();
});
