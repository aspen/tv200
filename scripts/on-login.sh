#!/bin/bash
set -euo pipefail

# For some reason LXTerminal when run with a command counts as a login and runs this script again,
# so to prevent infinite terminals we check if a terminal is already running.
if pidof lxterminal >/dev/null; then
  # Already running
  exit
fi

# Wait to give the X server time to start up
sleep 1

# Prevent screen locking
# (from https://forums.raspberrypi.com/viewtopic.php?t=147311)
xset s 0 0
xset s noblank
xset s noexpose
xset dpms 0 0 0

# Stop XScreenSaver
killall xscreensaver || true

# Move into script directory
cd "$( dirname "${BASH_SOURCE[0]}" )"

# --no-remote forces a new instance of LXTerminal to open. When it opens a new window on an existing
# instance and the exisitng instance was launched from a symlink (eg /usr/bin/x-terminal-emulator)
# then the pidof check for lxterminal at the top will fail.
lxterminal --no-remote -e "./pull-and-start-player.sh"
