#!/bin/bash
set -euo pipefail

# Some useful info on non-interactive pi configuration: https://raspberrypi.stackexchange.com/a/66939
# Probably best to consult the source: https://github.com/RPi-Distro/raspi-config/blob/master/raspi-config

echo "Updating system..."
sudo apt-get update
sudo apt dist-upgrade

echo "Enabling auto-updates..."
sudo apt-get install unattended-upgrades
sudo dpkg-reconfigure --priority=low unattended-upgrades

echo "Installing desktop environment..."
sudo apt install xserver-xorg lightdm raspberrypi-ui-mods libgles2-mesa libgles2-mesa-dev xorg-dev
sudo systemctl enable lightdm

echo "Setting auto-login to desktop"
# See https://github.com/RPi-Distro/raspi-config/blob/10431dc73d05031788a2f6a8981cf8c4ef9d0f33/raspi-config#L1398
# If this stops working, check if raspi-config has changed
AUTO_LOGIN_CODE=$(grep -Po 'B\d(?= Desktop Autologin")' "$(which raspi-config)")
sudo raspi-config nonint do_boot_behaviour "$AUTO_LOGIN_CODE"

MEM_SPLIT=512
echo "Setting GPU memory split to $MEM_SPLIT"
sudo raspi-config nonint do_memory_split "$MEM_SPLIT"

echo "Installing various packages..."
sudo apt install vim arandr chromium-browser youtube-dl

echo "Looking for HDMI audio sink"
HDMI_SINK="$(pacmd list-sinks | grep 'name:' | grep -Po '(?<=<).*hdmi.*(?=>)')"
echo "Setting audio out to $HDMI_SINK"
pacmd set-default-sink "$HDMI_SINK"

SCRIPT_PATH="/etc/profile.d/tv200.sh"
echo "Creating autostart script at $SCRIPT_PATH"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "$SCRIPT_DIR/on-login.sh &" | sudo tee "$SCRIPT_PATH"

echo "Setting timezone based on IP address"
sudo timedatectl set-timezone "$(curl http://ip-api.com/json/ | grep -Po '(?<="timezone":")[^"]*')"

echo "
Next you might want to set the display resolution to 1080p.

Remember to add extensions to chromium:
  - ublock:  https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm
  - h264ify: https://chrome.google.com/webstore/detail/h264ify/aleakchihdccplidncghkekgioiakgal

And enable these flags:
  - chrome://flags/#ignore-gpu-blocklist
  - chrome://flags/#enable-gpu-rasterization

And finally, always launch chromium with:
  - --enable-features=VaapiVideoDecoder

You can test if hardware accel is working at chrome://gpu.
Make sure everything is green except vulkan.

You may want to reboot"
