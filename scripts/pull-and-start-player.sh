#!/bin/bash
set -xeuo pipefail

# Move into script directory
cd "$( dirname "${BASH_SOURCE[0]}" )"

git pull

exec ./start-player.sh
