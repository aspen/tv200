#!/bin/bash
set -xeuo pipefail

# Move into project root directory
cd "$( dirname "${BASH_SOURCE[0]}" )/.."

# Detect the version of Chrome we have on the system
for i in chromium chromium-browser chrome google-chrome google-chrome-stable; do
  if which $i; then
    CHROME=$i
    break
  fi
done
if test -z $CHROME; then
  echo "No known Chrome installed on system"
  exit 1
fi

# If Chrome is closed improperly it will show an annoying "restore pages" dialog next time, get rid of that
# Credit: https://raspberrypi.stackexchange.com/a/89705
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/g' ~/.config/chromium/Default/Preferences

# The --kiosk flag does not allow us to be logged in to accounts, so don't use it

# Launch Chrome
# --load-extension: the tv200 browser extension that manages everything, including opening the home page
# --autoplay-policy=no-user-gesture-required: allows videos to autoplay, otherwise user input is required first
# --enable-features=VaapiVideoDecoder: enables video acceleration on the raspberry pi
$CHROME \
  --load-extension=player \
  --autoplay-policy=no-user-gesture-required \
  --start-fullscreen \
  --enable-features=VaapiVideoDecoder
