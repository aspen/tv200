const playerIdPrefix = 'tv200-'
const protocolVersion = 5;
const networkIdLength = 3;
const pairingCodeLength = 3;
let peer = null;
let conn = null;
let reopenOnError = false;
let pingTimeout = null;

function connValid() {
  return peer && !peer.destroyed && conn && conn.open;
}

function playerIsAlive() {
  clearTimeout(pingTimeout);
  pingTimeout = setTimeout(() => {
    pingPlayer();
  }, 25000);
}

function pingPlayer() {
  clearTimeout(pingTimeout);
  if (connValid()) {
    console.log('Pinging', conn.peer);
    conn.send({type: 'ping'});
    pingTimeout = setTimeout(() => {
      if (connValid()) {
        console.log('Did not get pong from ping');
        resetState(true, 'Timeout, Reconnecting...');
      }
    }, 5000);
  } else if (conn) {
    pingTimeout = setTimeout(() => {
      if (!connValid()) {
        console.log('Connecting timed out');
        resetState(true, 'Trying again...');
      }
    }, 5000);
  }
}

function ensurePeer(callback) {
  if (peer && !peer.destroyed && !peer.disconnected) {
    callback(peer);
  } else {
    console.log('Creating PeerJS peer...');
    if (peer && !peer.destroyed) {
      peer.destroy();
    }
    // Adding the following to the peer constructor gets rid of the "WebRTC: Using more than two
    // STUN/TURN servers slows down discovery" warnings on each peer construction, but doesn't seem
    // to measurably improve connection times:
    // {'config': {'iceServers': [{ 'urls': 'stun:stun.l.google.com:19302' }], 'sdpSemantics': 'unified-plan'}}
    // However generating our own peer ID instead of using one provided does improve connection
    // times by 0.5 - 1 seconds
    peer = new Peer('tv200-' + generateId());
    peer.on('error', error => {
      if (error.type === 'peer-unavailable') {
        resetState(false, 'TV was not available');
        focusIdInput();
      } else if (error.type === 'network') {
        console.log('Network error, may be fine');
        pingPlayer();
      } else if (error.type === 'browser-incompatible' && !navigator.onLine) {
        // For some reason we get a browser-incompatible error when offline, ignore
        resetState(false, 'Offline');
      } else {
        showError('Peer ' + error.type + ' error: ' + error.message);
      }
      // Don't reset state because may not be fatal
    });
    peer.on('connection', newConn => {
      console.error('Someone tried to connect to us ??');
      newConn.close();
    });
    peer.on('open', myId => {
      console.log('Peer opened with ID ' + myId);
      callback(peer);
    });
  }
}

// This function doesn't handle auth
// Callback is not called if connection fails
function tryOpenConnection(networkId, callback) {
  const playerId = playerIdPrefix + networkId;
  ensurePeer(peer => {
    if (conn && conn.open) {
      console.log('We already have a connection');
      return;
    }
    // The docs say that for fastest connection time we should not wait for the peer open event before connecting,
    // but empirically that doesn't work
    let newConn;
    try {
      newConn = peer.connect(playerId, {reliable: true});
    } catch (e) {
      // We get `DOMException: Can't create RTCPeerConnections when the network is down` when we try
      // to connect shortly after coming back online. If we're offline then autoconnect will be
      // triggered once we're back online. If we're online then poll.
      if (navigator.onLine) {
        setStatus('Polling...');
        setTimeout(() => resetState(true, 'Found network...'), 100);
      }
      return;
    }
    setStatus('Connecting...');
    pingPlayer();
    newConn.on('open', () => {
      if (conn && conn.open) {
        console.error('Ignoring new connection because we already have an open connection');
      } else {
        playerIsAlive();
        conn = newConn;
        callback(newConn);
      }
    });
    newConn.on('close', () => {
      console.log('Connection closed');
      if (reopenOnError) {
        resetState(true, 'Closed, reconnecting...');
      } else {
        resetState(false, 'Not connected');
      }
    });
    newConn.on('error', error => {
      console.log('Connection error: ' + error.message);
      if (reopenOnError) {
        resetState(true, 'Error, reconnecting...');
      } else {
        resetState(false, 'Error');
      }
    });
  });
}

function tryPair(pairingId) {
  if (pairingId.length != networkIdLength + pairingCodeLength) {
    showError('Pairing ID is wrong length');
    return;
  }
  const networkId = pairingId.substring(0, networkIdLength);
  const pairingCode = pairingId.substring(networkIdLength, pairingId.length);
  let authorized = false;
  setStatus('Pairing...');
  tryOpenConnection(networkId, newConn => {
    setStatus('Authorizing...');
    newConn.on('data', message => {
      if (authorized) {
        handleMessage(message);
      } else if (message.type === 'authorized') {
        authorized = true;
        handleAuthorized(networkId, message);
      } else {
        resetState(true, 'Invalid message after pairing');
      }
    });
    newConn.send({
      type: 'pair',
      code: pairingCode,
    });
    document.getElementById('media-div').style.display = 'flex';
  });
}

function forEachToken(func) {
  func('cid');
  func('p2c');
  func('c2p');
}

function tryAutoconnect() {
  if (conn && conn.open) {
    console.log('Ignoring autoconnect because we already have a connection');
    return;
  }
  const networkId = localStorage.getItem('network-id');
  if (!networkId) {
    console.log('No network ID, can\'t autoconnect');
    resetState(false);
    focusIdInput();
    return
  }
  tryOpenConnection(networkId, newConn => {
    setStatus('Authorizing...');
    // Giving an unauthorized TV our cid gives them the ability to get a p2c token from the player we think they are.
    // To prevent them doing that then using the p2c to authorize with us, we wipe out our tokens here.
    // That way we only try to use a set of tokens once.
    // PeerJS only allowing a single peer with a given name ensures a real-time MITM attack isn't possible.'
    tokens = {};
    gotP2c = false;
    authorized = false;
    forEachToken(name => {
      tokens[name] = localStorage.getItem(name + '-token');
      localStorage.removeItem(name + '-token');
    });
    localStorage.removeItem('network-id');
    newConn.on('data', message => {
      if (authorized) {
        handleMessage(message);
      } else if (gotP2c && message.type === 'authorized') {
        authorized = true;
        handleAuthorized(networkId, message);
      } else if (!gotP2c && message.type === 'autoconnect' && message.stage === 'p2c') {
        if (message.token === tokens.p2c) {
          gotP2c = true;
          newConn.send({
            type: 'autoconnect',
            stage: 'c2p',
            token: tokens.c2p,
          });
        } else {
          resetState(false, 'Invalid p2c token');
        }
      } else {
        resetState(false, 'Unexpected message during auth');
      }
    });
    newConn.send({
      type: 'autoconnect',
      stage: 'cid',
      token: tokens.cid,
    });
  });
}

function handleAuthorized(networkId, message) {
  console.log('Authorized by player ' + networkId);
  forEachToken(name => {
    localStorage.setItem(name + '-token', message[name]);
  });
  localStorage.setItem('network-id', networkId);
  updateConnectedState(true);
  reopenOnError = true;
  document.getElementById('peer-id-input').value = networkId + message.code;
  if (message.version !== protocolVersion) {
    const frag = document.importNode(document.getElementById('version-mismatch').content, true);
    frag.getElementById('version-mismatch-player-version').textContent = 'v' + message.version;
    frag.getElementById('version-mismatch-control-version').textContent = 'v' + protocolVersion;
    showError(frag);
  }
}

addEventListener('offline', () => {
  pingPlayer();
});

addEventListener('online', () => {
  if (!connValid()) {
    console.log('Now online, autoconnecting...');
    resetState(true, 'Back online...');
  }
});

addEventListener('beforeunload', () => {
  resetState(false);
});
